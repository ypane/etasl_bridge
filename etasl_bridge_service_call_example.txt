rosservice call /trace_spline "id: 0
type: 0
cartesianSequence:
- position: {x: 0.0, y: -0.7, z: 0.0}
  orientation: {x: 0.0, y: 0.0, z: 0.0, w: 0.0}
- position: {x: 0.1, y: -0.6, z: 0.0}
  orientation: {x: 0.0, y: 0.0, z: 0.0, w: 0.0}
- position: {x: 0.2, y: -0.5, z: 0.0}
  orientation: {x: 0.0, y: 0.0, z: 0.0, w: 0.0}
- position: {x: 0.3, y: -0.4, z: 0.0}
  orientation: {x: 0.0, y: 0.0, z: 0.0, w: 0.0}
- position: {x: 0.4, y: -0.3, z: 0.0}
  orientation: {x: 0.0, y: 0.0, z: 0.0, w: 0.0}
- position: {x: 0.5, y: -0.2, z: 0.0}
  orientation: {x: 0.0, y: 0.0, z: 0.0, w: 0.0}
- position: {x: 0.55, y: -0.1, z: 0.0}
  orientation: {x: 0.0, y: 0.0, z: 0.0, w: 0.0}
- position: {x: 0.55, y: 0.0, z: 0.0}
  orientation: {x: 0.0, y: 0.0, z: 0.0, w: 0.0}
- position: {x: 0.55, y: 0.1, z: 0.0}
  orientation: {x: 0.0, y: 0.0, z: 0.0, w: 0.0}
- position: {x: 0.5, y: 0.2, z: 0.0}
  orientation: {x: 0.0, y: 0.0, z: 0.0, w: 0.0}
- position: {x: 0.4, y: 0.3, z: 0.0}
  orientation: {x: 0.0, y: 0.0, z: 0.0, w: 0.0}
- position: {x: 0.3, y: 0.4, z: 0.0}
  orientation: {x: 0.0, y: 0.0, z: 0.0, w: 0.0}
- position: {x: 0.2, y: 0.5, z: 0.0}
  orientation: {x: 0.0, y: 0.0, z: 0.0, w: 0.0}
- position: {x: 0.1, y: 0.6, z: 0.0}
  orientation: {x: 0.0, y: 0.0, z: 0.0, w: 0.0}
- position: {x: 0.0, y: 0.7, z: 0.0}
  orientation: {x: 0.0, y: 0.0, z: 0.0, w: 0.0}
jointSequence:
- position: [0]
velocities: [0]"


