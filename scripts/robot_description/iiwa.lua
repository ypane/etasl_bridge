----------------------------------------------------------------
-- Author: Yudha Pane 
-- Institution/Company: Robotics Group, PMA, KU Leuven
-- Year: 2017
-- Description: Lua file for the KUKA iiwa description 
----------------------------------------------------------------


-- loading the robot with a standard script:
--
--  module that always defines the "robot_ee" and "robot_joints"
--  variables.
--  
--  "robot_ee" returns the robot end plate mounting frame ( the place on the
--  robot where you can mount tools and sensors on, z-axis is pointing
--  outwards)) with respect to the robot base.
--
-- "robot_joints" returns the robot joints in the order required by the driver for that robot.
--
-- usage:
--    require("robot_lwr")
--    now robot_ee should be defined.


require("context")
require("geometric")
require "ansicolors"

-- NOTE: The line below is still hardcoded
setup_selection = "IIWA14_BARE"

-- loading a model for the unversal robot lwr:
local u=UrdfExpr();

if (setup_selection == "FM") then
  u:readFromFile(rospack_find("assembly_application").."/urdf/iiwa7.urdf")
  u:addTransform("ee","iiwa_link_ee","world")
  u:addTransform("tool_frame","iiwa_link_ee","world") -- last joint w.r.t the base

  local r = u:getExpressions(ctx)
  robot_ee = r.ee
  ee = r.ee  -- other naming possibility 
  tool_frame      = r.tool_frame

elseif (setup_selection == "KUL") then 
  u:readFromFile(rospack_find("assembly_application").."/urdf/iiwa14withschunk.urdf")

  -- name ... result frame -- reference frame
  -- we control the tool frame with respect to the base_link with shorthand notation ee

  -- These links are currently present:
  -- world, iiwa_link_0, iiwa_link_1, ..., iiwa_link_7, iiwa_link_ee_kuka, iiwa_link_ee_pneumatic, us_tool_tip
  u:addTransform("T_flange_world","iiwa_link_ee_pneumatic","iiwa_link_0")
  u:addTransform("ee","iiwa_link_ee_pneumatic","iiwa_link_0") -- last joint w.r.t the base
  u:addTransform("world_wrt_wrist", "iiwa_link_0","iiwa_link_ee_pneumatic") -- base  w.r.t the last joint
  u:addTransform("tool_frame","iiwa_schunk_center_link","iiwa_link_0") -- last joint w.r.t the base



  local r = u:getExpressions(ctx)
  robot_ee = r.ee -- (define the end-effector frame)
  ee = r.ee  -- other naming possibility 
  tool_frame      = r.tool_frame
elseif (setup_selection == "IIWA14_BARE") then
  print(ansicolors.yellow .. "Loading bare iiwa14 description" .. ansicolors.reset)
  u:readFromFile(rospack_find("iiwa_description").."/urdf/iiwa14.urdf")

  -- name ... result frame -- reference frame
  -- we control the tool frame with respect to the base_link with shorthand notation ee

  -- These links are currently present
  -- world, iiwa_link_0, iiwa_link_1, ..., iiwa_link_7, iiwa_link_ee_kuka, iiwa_link_ee_pneumatic, us_tool_tip
  u:addTransform("ee","iiwa_link_ee_pneumatic","iiwa_link_0") -- last joint w.r.t the base
  u:addTransform("tool_frame","iiwa_link_ee_pneumatic","iiwa_link_0") -- last joint w.r.t the base



  local r = u:getExpressions(ctx)

  robot_ee 		= r.ee -- (define the end-effector frame)
  ee = r.ee  -- other naming possibility 
  tool_frame      = r.tool_frame
end


robot_joints={"iiwa_joint_1","iiwa_joint_2","iiwa_joint_3","iiwa_joint_4","iiwa_joint_5","iiwa_joint_6", "iiwa_joint_7"}


