require("rtt")
require("rttlib")

require("rttros")
require("kdlutils")
require("std_defs")

pkg_dir = ros:find("etasl_bridge")
skills_dir = ros:find("finrop_skills")
robot_spec_path = pkg_dir.."/scripts/robot_description/iiwa.lua"
spline_trace_spec_path = skills_dir.."/scripts/etasl/spline_example.lua"

tc          = rtt.getTC() 
depl        = tc:getPeer("Deployer")
etasl       = depl:getPeer("etasl")

return rfsm.state {
   stopped = rfsm.state {
      entry=function() 
            -- Ready pose
            etasl:stop()
      end,
   },
 
   trace_spline = rfsm.state {
      entry=function()
           etasl:readTaskSpecificationFile(robot_spec_path) -- load robot specification
           etasl:readTaskSpecificationFile(spline_trace_spec_path) -- load skill specification
           etasl:configure() -- configure the parameter of the spline motion skill
           etasl:initialize()
           etasl:start()
      end,
   },
  
   peg_in_the_hole = rfsm.state {
      entry=function()  
           etasl:readTaskSpecificationFile(robot_spec_path) -- load robot specification
           etasl:configure() -- configure with the parameters of the peg in the hole skill
           etasl:initialize()
           etasl:start()
      end,

   },

   rfsm.trans {src="initial", tgt="stopped" },
   rfsm.trans {src="stopped", tgt="trace_spline", events={"e_trace_spline"}},
   rfsm.trans {src="stopped", tgt="peg_in_the_hole", events={"e_peg_in_the_hole"}},
   
   rfsm.trans {src="trace_spline", tgt="stopped", events={"e_finished@etasl"}},
   rfsm.trans {src="peg_in_the_hole", tgt="stopped", events={"e_finished@etasl"}},
}

