#include "etasl_bridge-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>
#include <fstream>


Etasl_bridge::Etasl_bridge(std::string const& name) : TaskContext(name),
change_motion_state_oc("changeState")
{
  /* Add operation caller */
  this->requires()->addOperationCaller(change_motion_state_oc);
  
  /* Add IO ports */
  this->addPort("events_in", events_inport).doc("Event input port to receive status of the skill");
  this->addPort("events", events_outport).doc("Event output port to notify the scheduler");
  this->addPort("skill_result", skill_result_outport).doc("Output port to stream the message of skill execution status");
  
  /* Add operations */
  this->addOperation("trace", &Etasl_bridge::trace, this, OwnThread)
  .doc("trace operation (dummy) ");
  this->addOperation("closeFRI", &Etasl_bridge::closeFRI, this, OwnThread)
  .doc("close the FRI connection ");
  this->addProperty("filePath_pos", filePath_pos).doc("the csv file containing spline waypoints for position");
  this->addProperty("filePath_rot", filePath_rot).doc("the csv file containing spline waypoints for rotation");
  this->addProperty("serviceTimeout", serviceTimeout).doc("timeout for the skill service to return its status [seconds]");
  
  // Initialize variables
  filePath_pos = "/home/spline_example_pos.csv";
  filePath_rot = "/home/spline_example_rot.csv";
  serviceTimeout = 10; 
  skill_status = false;
  skill_done = false;
  skill_id = 999;
  std::cout << "Etasl_bridge constructed !" <<std::endl;
}

bool Etasl_bridge::configureHook(){
  std::cout << "Etasl_bridge configured !" <<std::endl;
  return true;
}       

bool Etasl_bridge::startHook(){
  std::cout << "Etasl_bridge started !" <<std::endl;
  return true;
}

void Etasl_bridge::updateHook(){
   if ( events_inport.read(skill_status_msg)== RTT::NewData)
   {
      if ((skill_status_msg.compare("e_finished@etasl") == 0) || (skill_status_msg.compare("e_finished@etasl_peg_in_hole") == 0))
      {
         skill_status = true;
         skill_done = true;
      }
      else if (skill_status_msg.compare("failed") == 0)
      {
         skill_status = false;
         skill_done= true;
      }
   }
   
   if (skill_done)
   {
       if (skill_status)
       {
            skill_result_msg.id = skill_id;
            skill_result_msg.success=true;
            skill_result_msg.error = "none";
            skill_result_outport.write(skill_result_msg);
       }
       else
       {
            skill_result_msg.id = skill_id;
            skill_result_msg.success=false;
            skill_result_msg.error = "unknown";
            skill_result_outport.write(skill_result_msg);           
       }
   }       
       
  elapsed_time =  RTT::os::TimeService::Instance()->secondsSince( start_time);
  
}

void Etasl_bridge::stopHook() {
  std::cout << "Etasl_bridge executes stopping !" <<std::endl;
}

void Etasl_bridge::cleanupHook() {      
  std::cout << "Etasl_bridge cleaning up !" <<std::endl;
}

bool Etasl_bridge::closeFRI() {
   // set skill finished status
   skill_done = false;

   // call the ROS service through an operation caller
   if (change_motion_state_oc.ready())
   {
      finrop_msgs::ChangeMotionState::Request req;
      finrop_msgs::ChangeMotionState::Response res;
      
      req.id = 0; // not used
      req.action = 1; // START is 0 and STOP is 1
      req.type = 1; // FRI is type 1
      req.stiffness = 1000; // Nm/rad
      req.damping = 0.7; // Nms/rad
            
      // call the service
      change_motion_state_oc(req, res);
      
      
      std::cout << "FRI close command is called !" <<std::endl;

      return true;
   }
   else
   {
      std::cout << "The ChangeMotionState service is not ready!" <<std::endl;
      return false;
   }
}

bool Etasl_bridge::trace(finrop_msgs::Trace::Request &req, finrop_msgs::Trace::Response &res)
{
   std::cout << "Operation is called with parameters: " << req.id << std::endl;
   // WARNING: Hard-coded tracking speed
   int n_waypoints = req.cartesianSequence.size();
   std::cout << "Size of waypoints: " << n_waypoints << std::endl;
   
   double speed = 0.1; // [m/s]
   double time_stamp = 0.0;
   double prev_time_stamp = 0.0;
   
   // Generate the waypoints in a .CSV file
   // The parameters of the spline skill includes:
   // a) array of cartesian pose
   // b) the speed of of the spline motion (needs to be added to the Trace.srv file)
   
   // Store the id
   skill_id = req.id;
   
   ofstream myfile_pos;
   ofstream myfile_rot;
   std::cout << "filepaths: " << filePath_pos.c_str() << "   " << filePath_rot.c_str() << std::endl;
   
   /* Write csv file for position */
   myfile_pos.open (filePath_pos.c_str());
   myfile_rot.open (filePath_rot.c_str());
   for (int i=0; i<n_waypoints; i++)
   {      

      std::cout << "waypoints[" << i << "]=" << req.cartesianSequence[i].position.x << "  " << req.cartesianSequence[i].position.y << "  " << req.cartesianSequence[i].position.z << std::endl;
      if (i == 0)
      {
         time_stamp = 0.0;
      }
      else
      {
         double dx = req.cartesianSequence[i].position.x-req.cartesianSequence[i-1].position.x;
         double dy = req.cartesianSequence[i].position.y-req.cartesianSequence[i-1].position.y;
         double dz = req.cartesianSequence[i].position.z-req.cartesianSequence[i-1].position.z;
         double d  = sqrt(dx*dx + dy*dy + dz*dz);
         time_stamp = prev_time_stamp + d/speed;
         prev_time_stamp = time_stamp;
      }

      // Conversion from quaternion to euler by instantiating a KDL frame object
      waypoint.p.x(req.cartesianSequence[i].position.x);
      waypoint.p.y(req.cartesianSequence[i].position.x);
      waypoint.p.z(req.cartesianSequence[i].position.x);
      waypoint.M = KDL::Rotation::Quaternion(req.cartesianSequence[i].orientation.x, req.cartesianSequence[i].orientation.y, req.cartesianSequence[i].orientation.z, req.cartesianSequence[i].orientation.w);
      
      // Get euler/rpy angles
      double roll_angle = 0;
      double pitch_angle = 0;
      double yaw_angle = 0;
      waypoint.M.GetRPY(roll_angle, pitch_angle, yaw_angle); // NOTE: Does etasl use RPY or Euler ZYX?

      myfile_pos << time_stamp <<  " , " << req.cartesianSequence[i].position.x << " , " << req.cartesianSequence[i].position.y << " , "
                                   << req.cartesianSequence[i].position.z << "\n";            
      myfile_rot << time_stamp <<  " , " <<  roll_angle  << " , " << pitch_angle << " , " << yaw_angle  << "\n";            

   }
    // close the csv files
   myfile_pos.close();
   myfile_rot.close();
   
    // Send activation event to the scheduler
    events_outport.write("e_trace_spline");  
        
    start_time = RTT::os::TimeService::Instance()->getTicks();

//     while ((elapsed_time < serviceTimeout) || (!skill_status)) {
//        res.status = false;
//     }
// 
//     res.status = true;
    return true;

}


/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Etasl_bridge)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Etasl_bridge)
