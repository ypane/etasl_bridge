#ifndef OROCOS_ETASL_BRIDGE_COMPONENT_HPP
#define OROCOS_ETASL_BRIDGE_COMPONENT_HPP

#include <rtt/TaskContext.hpp>
#include <rtt/Port.hpp>
#include <std_msgs/Float64.h>
#include <std_msgs/String.h>
#include <std_srvs/Empty.h>
#include <ros/ros.h>
#include <finrop_msgs/Trace.h>
#include <finrop_msgs/ChangeMotionState.h>
#include <finrop_msgs/SkillResultRequest.h>
#include <geometry_msgs/PoseArray.h>
#include <kdl/frames.hpp>

using namespace RTT;
using namespace std;

class Etasl_bridge : public RTT::TaskContext{
public:
   Etasl_bridge(std::string const& name);
   bool configureHook();
   bool startHook();
   void updateHook();
   void stopHook();
   void cleanupHook();
private:
   InputPort <std::string> events_inport;
   OutputPort <std::string> events_outport; 
   OutputPort <finrop_msgs::SkillResultRequest> skill_result_outport;
   
   string filePath_pos;
   string filePath_rot;
   KDL::Frame waypoint;
   float serviceTimeout;  
   RTT::os::TimeService::ticks start_time;
   float elapsed_time; // elapsed time since a skill service is called [seconds]
   std::string skill_status_msg; // the skill will send an event once it is successfully executed / failed
   bool skill_status; // true if success, false if failed
   finrop_msgs::SkillResultRequest skill_result_msg;
   int skill_id; 
   bool skill_done; // true if the skill execution is finished
   
//   OperationCaller<bool(std_srvs::TraceAction::Request&, std_srvs::TraceAction::Response&)> trace_spline_oc; 
//   bool traceSpline(std::finrop_msgs::TraceAction::Request &req, std::finrop_msgs::TraceAction::Response &res);
   OperationCaller<bool(finrop_msgs::ChangeMotionState::Request&, finrop_msgs::ChangeMotionState::Response&)> change_motion_state_oc; 
   bool trace(finrop_msgs::Trace::Request &req, finrop_msgs::Trace::Response &res);
   bool openFRI();
   bool closeFRI();
};
#endif
