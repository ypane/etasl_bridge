# ETASL BRIDGE PACKAGE

This package contains a Orocos RTT package that provides the bridge from the Sunrise Java application to eTaSL

## Getting Started

Clone this project to your ROS workspace source directory

```
cd ~/foo_ws/src
```
```
git clone bitbucket.
```
or

### Prerequisites

1. `finrop_msgs`: can be requested from Author
2. `rtt_finrop_msgs`: generated from `finrop_msgs` by typing inside the folder `~/foo_ws/src` or `~/foo_ws/src/finrop`:

```
rosrun rtt_roscomm create_rtt_msgs finrop_msgs
```


### Compiling
```
cd ~/foo_ws/src
```

```
catkin_make
```
If it fails to locate header(s) file(s) from the `finrop_msgs` package, then try to install instead
```
catkin_make install
```
and update the `ROS_PACKAGE_PATH` variable
```
export ROS_PACKAGE_PATH="/home/myname/foo_ws/install:$ROS_PACKAGE_PATH"
```

## Running the tests

## Authors

* **Yudha Pane** 

## License
To be used by KU Leuven ROB group members or Flanders Make within the FINROP project only

## Acknowledgments

