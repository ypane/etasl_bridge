require("rttlib")
require("rttros")
require("kdlutils")
require("std_defs")

-- Set variables
change_motion_state_service_name = "/iiwa/changeMotionState" -- TODO: update this


pkg_dir = ros:find("etasl_bridge")
simulation = false

tc = rtt.getTC() -- Task context
depl = tc:getPeer("Deployer") -- The deployer for import 
gs = rtt.provides() -- Global service object. To access global functions.
cp = rtt.Variable("ConnPolicy")


depl:import("rtt_ros")
ros = gs:provides("ros")
depl:import("rtt_rospack")
depl:import("rtt_rosnode")
depl:import("rtt_roscomm")
depl:import("rtt_visualization_msgs")
depl:import("rtt_finrop_msgs")


depl:import("etasl_bridge")
depl:loadComponent("etasl_bridge", "Etasl_bridge")
etasl_bridge = depl:getPeer("etasl_bridge")

depl:loadService("etasl_bridge", "rosservice")
etasl_bridge:configure() 
etasl_bridge:getProperty("filePath"):set("/home/patricia/etasl_workspace/src/etasl_bridge/spline_example.csv")
if simulation then
   etasl_bridge:provides("rosservice"):connect("trace", "/trace_spline", "finrop_msgs/Trace")
else       
   etasl_bridge:provides("rosservice"):connect("trace", "/etasl/command/TraceRequest", "finrop_msgs/Trace")
end

etasl_bridge:start()

-- Etasl component:
-- solver
ros:import("etasl_solver_qpoases")
depl:loadComponent("solver","etasl_solver_qpoases")
solver = depl:getPeer("solver")

-- jointstate I/O factories
ros:import("etasl_iohandler_jointstate")
depl:loadComponent("joint_io","Etasl_IOHandler_Jointstate")
joint_io = depl:getPeer("joint_io") 
    
depl:import("etasl_rtt")
depl:loadComponent("etasl", "etasl_rtt")
etasl = depl:getPeer("etasl")
depl:connectPeers("etasl","solver")
depl:connectPeers("etasl","joint_io")
solver:create_and_set_solver("etasl")  

-- etasl common ports
jn = s{ "iiwa_joint_1", "iiwa_joint_2", "iiwa_joint_3", 
         "iiwa_joint_4", "iiwa_joint_5", "iiwa_joint_6", 
         "iiwa_joint_7", "iiwa_slider_xpos_joint","iiwa_slider_xneg_joint"}
etasl:add_controller_inputport("jointpos", "Joint Position Values", jn)
etasl:add_controller_outputport("jointvel", "Joint velocity values", jn)
etasl:add_controller_motion_control_msgs_joint_velocities_outputport("qdotd","Joint velocity values",jn) 
depl:connectPeers("etasl","joint_io")
joint_io:add_controller_jointstate_outputport("etasl","joint_state","Joint state values",jn)
depl:stream("etasl.joint_state", ros:topic("/joint_states"))
depl:setActivity("etasl", 0.01, 99, rtt.globals.ORO_SCHED_RT)


-- Robot component
depl:loadComponent("robot", "OCL::LuaComponent")
robot = depl:getPeer("robot")
robot:exec_file(rtt.provides("ros"):find("finrop_skills").."/scripts/rtt/simple_robot_sim.lua")
init_jnts = d{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}
robot:getProperty("initial_position"):set(init_jnts)
robot:configure()
depl:setActivity("robot", 0.01, 99, rtt.globals.ORO_SCHED_RT)
robot:start()

-- Supervisor component
depl:loadComponent("Supervisor", "OCL::LuaComponent")
sup = depl:getPeer("Supervisor")
sup:exec_file(pkg_dir.."/scripts/scheduler/fsm_component.lua")
sup:getProperty("state_machine"):set(pkg_dir.."/scripts/scheduler/fsm_scheduler.lua")
sup:addPeer(depl)
sup:configure()
sup:start()

-- Robot-etasl connection
depl:connect("etasl.jointvel","robot.jointvel",cp ) 
depl:connect("robot.jointpos","etasl.jointpos",cp )

-- etasl_bridge-supervisor connection
depl:connect("etasl_bridge.events", "Supervisor.events", cp)

-- Dummy event port trigger
outport_event = rtt.OutputPort("string")
outport_event:connect(sup:getPort("events"))

-- connect Peers 
-- depl:connectPeers("Supervisor", "etasl")

-- start scheduler
sup:start()
